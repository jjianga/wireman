package com.zhkj.wireman.unit;

import java.math.BigDecimal;

public class UnitConversion {
    BigDecimal value;

    public UnitConversion() {
    }

    public UnitConversion(double value) {
        this.value  = new BigDecimal(value);
    }

    public static double format(double value,int newScale){
        return new BigDecimal(value).setScale( newScale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     *  获取基础单位的值
     * @return  值
     */
    public double getValue() {return value.doubleValue();}

    /**
     *  设置基础单位的值
     * @param value 值
     */
    public void setValue(double value) {this.value  = new BigDecimal(value);}

    /**
     *  向上单位转换值
     * @return getG，getM，getK 向大的单位转换
     */
    public double getG(){return getM() / 1000;}
    public double getM(){return getK() / 1000;}
    public double getK(){return getValue() / 1000;}

    /**
     *  向下单位转换
     * @return get_m，get_μ，get_n，get_p
     */
    public double get_m(){return getValue() * 1000;}
    public double get_μ(){return get_m() * 1000;}
    public double get_n(){return get_μ() * 1000;}
    public double get_p(){return get_n() * 1000;}

    /**
     * 设置带单位的值  向下自动转换到基础值
     * @param g 单位
     */
    public UnitConversion setG(double g){setM(g * 1000);return this;}
    public UnitConversion setM(double M){setK(M * 1000);return this;}
    public UnitConversion setK(double k){setValue(k * 1000);return this;}

    /**
     *  设置带单位的值 向上自动转换到基础置
     * @param m 单位
     */
    public UnitConversion set_m(double m){setValue(m / 1000);return this;}
    public UnitConversion set_μ(double μ){set_m(μ / 1000);return this;}
    public UnitConversion set_n(double n){set_μ(n / 1000);return this;}
    public UnitConversion set_p(double p){set_n(p / 1000);return this;}
}
