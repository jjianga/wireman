package com.zhkj.wireman;

/**
 * 分压计算器  未完成 反算有点复杂
 */
public class VoltageDividerUtils {
    /**
     *  已知 输入电压 vin， 电阻 r1， 电阻 r2  求输出电压
     * @param vin   电压
     * @param r1    电阻
     * @param r2    电阻
     * @return 电压
     */
    public static double getVinR1R2Vout(double vin, double r1, double r2){
        double r1R2 = DoubleUtils.add(r1, r2);
        double r2_r1R2 = DoubleUtils.divide(r2, r1R2);
        return DoubleUtils.mul(vin, r2_r1R2);
    }

    public static double getVinVoutR1R2(double vin, double vout, double r2){

        return 0.1;
    }

    public static void main(String[] args) {
        System.out.println(getVinR1R2Vout(5.0, 1, 1));
    }
}
