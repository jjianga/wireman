package com.zhkj.wireman;

/**
 * 电池寿命计算
 */
public class BatteryLifeUtils {
    /**
     *  计算电池寿命
     * @param Ah    电池容量 Ah
     * @param run_A 运行时电流 A
     * @param run_s 运行时间 s
     * @param noLoad_A  空载电流 A
     * @param noLoad_s  空载时间 s
     * @param await_A   待机电流 A
     * @param await_s   待机时间 s
     * @return
     */
    public static double getBatteryLifeTime(double Ah, double run_A, double run_s, double noLoad_A, double noLoad_s,
                                        double await_A, double await_s){
        double i = getBatteryLife(run_A, run_s, noLoad_A, noLoad_s, await_A, await_s);
        return DoubleUtils.divide(Ah, i, 2);
    }

    /**
     *  计算平均电流的消耗
     * @param run_A 运行时电流 A
     * @param run_s 运行时间 s
     * @param noLoad_A  空载电流 A
     * @param noLoad_s  空载时间 s
     * @param await_A   待机电流 A
     * @param await_s   待机时间 s
     * @return  平均电流消耗
     */
    public static double getBatteryLife(double run_A, double run_s, double noLoad_A, double noLoad_s,
                                        double await_A, double await_s){
        //运行时每秒消耗
        double run = DoubleUtils.divide(run_A, 60.0);
        //空载时每秒消耗
        double noLoad = DoubleUtils.divide(noLoad_A, 60.0);
        //待机时每秒消耗
        double await = DoubleUtils.divide(await_A, 60.0);

        // 计算 一个小时可运行次数
        double runA = DoubleUtils.mul(run, run_s);
        double noLoadA = DoubleUtils.mul(noLoad, noLoad_s);
        double awaitA = DoubleUtils.mul(await, await_s);

        double avv = DoubleUtils.add(runA, noLoadA);
        double all = DoubleUtils.add(avv, awaitA);
        double runNoLoadT = DoubleUtils.add(run_s, noLoad_s);
        double runNoLoadAwaitT = DoubleUtils.add(runNoLoadT, await_s);
        double ct = DoubleUtils.divide(all, runNoLoadAwaitT,3);
        return DoubleUtils.mul(ct, 60.0);
    }

    public static void main(String[] args) {
        System.out.println(getBatteryLife(100, 30, 1,30,0,0));
        System.out.println(getBatteryLifeTime(100,100, 30, 1,30,0,0));
    }
}
