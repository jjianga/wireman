package com.zhkj.wireman;

public class CapacitanceRechargeUtils {
    /************************************计算电容充电时间
    /**
     *  根据 rc 常数， r 电阻 获取 c 电容
     * @param rc 常数
     * @param r 电阻
     * @return c 电容
     */
    public static double getRcRC(double rc, double r)throws Exception{
        if(rc <= 0.0){
            throw new Exception("rc 是常数必须大于0");
        }
        if(r <= 0.0){
            throw new Exception("r 值必须大于 0 欧");
        }
        return DoubleUtils.divide(rc, r);
    }

    /**
     *  根据 c 电容 ， r 电阻 获取  rc 常数
     * @param c 电容
     * @param r 电阻
     * @return rc 常数
     * @throws Exception
     */
    public static double getCRcC(double c, double r)throws Exception{
        if(c <= 0.0){
            throw new Exception("c 电容必须大于0");
        }
        if(r <= 0.0){
            throw new Exception("r 值必须大于 0");
        }
        return DoubleUtils.mul(c, r);
    }

    /**
     *  根据  rc 常数 ， c 电容 获取  r 电阻
     * @param rc 常数
     * @param c 电容
     * @return r 电阻
     * @throws Exception
     */
    public static double getRRcR(double rc, double c)throws Exception{
        if(rc <= 0.0){
            throw new Exception("rc 是常数必须大于0");
        }
        if(c <= 0.0){
            throw new Exception("c 值必须大于 0 欧");
        }
        return DoubleUtils.divide(rc, c);
    }
    /***************************************** 求能量 和电流
    /**
     *  根据 t 常数， r 电阻， c 电容 vb 电压  求能量
     * @param t 常数
     * @param r 电阻
     * @param c 电容
     * @param vb 电压
     * @return q 求能量
     */
    public static double getQ(double t, double r, double c, double vb){
        double e = Math.E;//自然bai常数e的近似值
        double rc = DoubleUtils.mul(r, c);
        double trc = DoubleUtils.divide(t,rc);
        double etrc = Math.pow(e, -trc);
        double vetrc = DoubleUtils.sub(1.00, etrc);
        double cvb = DoubleUtils.mul(c, vb);
        return  DoubleUtils.mul(cvb, vetrc);
    }

    /**
     *  根据 t 常数， r 电阻， c 电容 vb 电压  求电流 i
     * @param t 常数
     * @param r 电阻
     * @param c 电容
     * @param vb 电压
     * @return i 求电流
     */
    public static double getI(double t, double r, double c, double vb){
        double e = Math.E;//自然bai常数e的近似值
        double rc = DoubleUtils.mul(r, c);
        double trc = DoubleUtils.divide(t,rc);
        double etrc = Math.pow(e, -trc);
        double vbr = DoubleUtils.divide(vb, r);
        return  DoubleUtils.mul(vbr, etrc);
    }

    public static void main(String[] args) {
        System.out.println(getQ(10.0, 10.0, 1.0, 10.0));
        System.out.println(getI(10.0, 10.0, 1.0, 10.0));
    }
}
