package com.zhkj.wireman;

/**
 * 功率计算器
 */
public class PowerElectricalUtils {
    /*********************************直流电 求功率
    /***
     *  电压 v, 电流 i, 求功率 w
     * @param v 电压
     * @param a 电流
     * @return w 功率
     */
    public static double getDC_VAW(double v, double a){
        return DoubleUtils.mul(v, a);
    }

    /**
     * 功率 w, 电流 i, 求电压 v
     * @param w 功率
     * @param a 电流
     * @return v 电压
     */
    public static double getDC_WAV(double w, double a){
        return DoubleUtils.divide(w, a);
    }

    /**
     *  功率 w, 电压 v, 求电流 i
     * @param w 功率
     * @param v 电压
     * @return a电流
     */
    public static double getDC_WVA(double w, double v){
        return DoubleUtils.divide(w, v);
    }

    /********************************* 交流电
    /**
     *  求视在功率
     * @param v
     * @param a
     * @return
     */
    public static double getAC_VAVa(double v, double a){
        return DoubleUtils.mul(v, a);
    }

    /**
     *  求电流
     * @param Va 视在功率
     * @param v 电压
     * @return a 电流
     */
    public static double getAC_VaVA(double Va, double v){
        return DoubleUtils.divide(Va, v);
    }

    /**
     * 求电压
     * @param Va 视在功率
     * @param a 电压
     * @return v 电压
     */
    public static double getAC_VaAV(double Va, double a){
        return DoubleUtils.divide(Va, a);
    }

    /**
     *  求有功功率 单位 w  交流三相  一相算法一样
     * @param va 视在功率 va  单位
     * @param pf 功率因数
     * @return p 有功功率 单位 w
     */
    public static double getAC_VaPfP(double va, double pf){
        return DoubleUtils.mul(va,pf);
    }

    /**
     *  p 有功功率 单位 w, 功率因数 pf, 电压 v, 求电流 i
     * @param p 有功功率 单位 w
     * @param pf 功率因数
     * @param v 电压
     * @return a 电流
     */
    public static double getAC_PPfVA(double p, double pf, double v){
        double s = DoubleUtils.divide(p, pf);
        return DoubleUtils.divide(s,v);
    }

    /**
     *  p 有功功率 单位 w, 功率因数 pf, 电流 i, 求电压 v
     * @param p 有功功率 单位 w
     * @param pf 功率因数
     * @param a 电流
     * @return v 电压
     */
    public static double getAC_PPfAV(double p, double pf, double a){
        double s = DoubleUtils.divide(p, pf);
        return DoubleUtils.divide(s, a);
    }
    /**
     *  p 有功功率 单位 w, 电压 v, 电流 i, 求功率因数 pf
     * @param p 有功功率 单位 w
     * @param v 电压
     * @param a 电流
     * @return pf 功率因数
     */
    public static double getAC_PVAPf(double p, double v, double a){
        double s = DoubleUtils.mul(v, a);
        return DoubleUtils.divide(p, s);
    }

    /**
     * 功率因数 转 相位
     * @param pf 功率因数
     * @return bs 相位
     */
    public static double pfToBs(double pf){
        return Math.toDegrees(Math.acos(pf));
    }

    /**
     *  求无功功率 单位 VAR 交流三相  一相算法一样
     * @param va 视在功率 va  单位
     * @param bs 相 φ 单位 度
     * @returna q 无功功率 单位 VAR
     */
    public static double getAC_VaBsQ(double va, double bs){
        return DoubleUtils.mul(va, Math.sin(Math.toRadians(bs)));
    }

    /**
     *  求无功功率 单位 VAR 的电流
     * @param q  无功功率 单位 VAR
     * @param bs 相 φ 单位 度
     * @param v 电压 v
     * @return a 电流
     */
    public static double getAC_QBsVA(double q, double bs, double v){
        double s = DoubleUtils.divide(q, Math.sin(Math.toRadians(bs)));
        return DoubleUtils.divide(s,v);
    }

    /**
     * 求无功功率 单位 VAR 的电压
     * @param q 无功功率 单位 VAR
     * @param bs 相 φ 单位 度
     * @param a 电流
     * @returnv v 电压
     */
    public static double getAC_QBsAV(double q, double bs, double a){
        double s = DoubleUtils.divide(q,  Math.sin(Math.toRadians(bs)));
        return DoubleUtils.divide(s, a);
    }
    /**
     * 求无功功率 单位 VAR 的相位
     * @param q 无功功率 单位 VAR
     * @param v 电压
     * @param a 电流
     * @returnv bs 相位
     */
    public static double getAC_QVABs(double q, double v, double a){
        double s = DoubleUtils.mul(v, a);
        return Math.toDegrees(Math.asin(DoubleUtils.divide(q, s)));
    }

    /**
     *  相位转 功率因数
     * @param bs 相 φ 单位 度
     * @return pf 功率因数
     */
    public static double bsToPf(double bs){
        return Math.cos(Math.toRadians(bs));
    }


    /********************************* 交流三相
     /**
     *  交流三相 求视在功率
     * @param v 电压
     * @param a 电流
     * @return va 三相 视在功率
     */
    public static double getAC3_VAVa(double v, double a){
        double va = DoubleUtils.mul(v, a);
        return DoubleUtils.mul(va, Math.sqrt(3.0));
    }

    /**
     * 交流三相 求电流
     * @param va 视在功率
     * @param v 电压
     * @return a 电流
     */
    public static double getAC3_VaVA(double va, double v){
        va = DoubleUtils.divide(va,  Math.sqrt(3.0));
        return DoubleUtils.divide(va, v);
    }

    /**
     *  交流三相 求电压 v
     * @param va 视在功率
     * @param a 电流
     * @return v 电压
     */
    public static double getAC3_VaAV(double va, double a){
        va = DoubleUtils.divide(va,  Math.sqrt(3.0));
        return DoubleUtils.divide(va, a);
    }

    /**
     * 交流三相  p 有功功率 单位 w, 功率因数 pf, 电压 v, 求电流 i
     * @param p 有功功率 单位 w
     * @param pf 功率因数
     * @param v 电压
     * @return a 电流
     */
    public static double getAC3_PPfVA(double p, double pf, double v){
        double va = DoubleUtils.divide(p, pf);
        va = DoubleUtils.divide(va,  Math.sqrt(3.0));
        return DoubleUtils.divide(va, v);
    }

    /**
     *  交流三相  p 有功功率 单位 w, 功率因数 pf, 电流 i, 求电压 v
     * @param p 有功功率 单位 w
     * @param pf 功率因数
     * @param a 电流
     * @return v 电压
     */
    public static double getAC3_PPfAV(double p, double pf, double a){
        double va = DoubleUtils.divide(p, pf);
        va = DoubleUtils.divide(va,  Math.sqrt(3.0));
        return DoubleUtils.divide(va, a);
    }
    /**
     *  交流三相  p 有功功率 单位 w, 电压 v, 电流 i, 求功率因数 pf
     * @param p 有功功率 单位 w
     * @param v 电压
     * @param a 电流
     * @return pf 功率因数
     */
    public static double getAC3_PVAPf(double p, double v, double a){
        double va = DoubleUtils.mul(v, a);
        va = DoubleUtils.mul(va, Math.sqrt(3.0));
        return DoubleUtils.divide(p, va);
    }

    /**
     *  求无功功率 单位 VAR 的电流
     * @param q  无功功率 单位 VAR
     * @param bs 相 φ 单位 度
     * @param v 电压 v
     * @return a 电流
     */
    public static double getAC3_QBsVA(double q, double bs, double v){
        double va = DoubleUtils.divide(q, Math.sin(Math.toRadians(bs)));
        va = DoubleUtils.divide(va, Math.sqrt(3.0));
        return DoubleUtils.divide(va, v);
    }

    /**
     * 求无功功率 单位 VAR 的电压
     * @param q 无功功率 单位 VAR
     * @param bs 相 φ 单位 度
     * @param a 电流
     * @returnv v 电压
     */
    public static double getAC3_QBsAV(double q, double bs, double a){
        double va = DoubleUtils.divide(q,  Math.sin(Math.toRadians(bs)));
        va = DoubleUtils.divide(va, Math.sqrt(3.0));
        return DoubleUtils.divide(va, a);
    }
    /**
     * 求无功功率 单位 VAR 的相位
     * @param q 无功功率 单位 VAR
     * @param v 电压
     * @param a 电流
     * @returnv bs 相位
     */
    public static double getAC3_QVABs(double q, double v, double a){
        double va= DoubleUtils.mul(v, a);
        va = DoubleUtils.mul(va, Math.sqrt(3.0));
        return Math.toDegrees(Math.asin(DoubleUtils.divide(q, va)));
    }



    public static void main(String[] args) {
        //25 * Math.PI

        // 有功功率 单位 w
        System.out.println(10000 * Math.cos(Math.toRadians(25)));
        // 无功功率 单位 VAR
        System.out.println(10000 * Math.sin(Math.toRadians(25)));
        //功率因数
        System.out.println( Math.cos(Math.toRadians(25)));
        System.out.println((10000 * Math.cos(Math.toRadians(25))) /10000);
        System.out.println(Math.toRadians(25));
        System.out.println(Math.toDegrees(Math.acos(Math.cos(Math.toRadians(25)))));
        System.out.println(getAC_PPfVA(90, 0.906, 10));
        System.out.println(getAC_PVAPf(90, 10, 10));
        System.out.println(pfToBs(0.9));
        System.out.println(getAC_QVABs(90,20.647, 10));
        System.out.println(getAC3_VAVa(10, 10));
        System.out.println(getAC3_VaAV(173.205, 10));
        System.out.println(getAC_VaPfP(100, 0.9));
        System.out.println(getAC_VaBsQ(173.205, 25.842));
        System.out.println(getAC3_PPfVA(155.885, 0.9, 10));
        System.out.println(getAC3_PVAPf(155.885, 10, 10));
        System.out.println(getAC3_QBsVA(754.498, 25.842, 10));
        System.out.println(getAC3_QBsAV(754.498, 25.842, 10));
        System.out.println(getAC3_QVABs(75.498, 10 , 10));
//        toDegrees

    }

}
