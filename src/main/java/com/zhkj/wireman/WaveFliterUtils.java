package com.zhkj.wireman;

import com.zhkj.wireman.unit.UnitConversion;

/***
 * 滤波器相关公式算法
 * @author Administrator
 *
 */
public class WaveFliterUtils {
	static Double PI = Math.PI;
	static Double PI2 = PI * 2;

	/********RC滤波器相关计算******************
	 /***
	 * 计算RC滤波器频率方法
	 * @param r  电阻
	 * @param c  电容
	 * @return f 频率
	 */
	public static double getRCF(double r, double c) {
		/// f = 1/(PI2 * r * c);
		double rc = DoubleUtils.mul(r, c);
		double prc = DoubleUtils.mul(PI2, rc);
		return DoubleUtils.divide(1.0, prc);
	}
	/***
	 * 计算RC滤波器电容方法
	 * @param f 频率
	 * @param r 电阻
	 * @return c 电容
	 */
	public static double getRCC(double f, double r) {
		// c = 1 / f / PI2 / r;
		double pic = DoubleUtils.divide(1.0, f, 10);
		double fc = DoubleUtils.divide(pic, PI2, 10);
		return DoubleUtils.divide(fc, r);
	}
	/****
	 * 计算RC滤波器电阻方法
	 * @param f 频率
	 * @param c 电容
	 * @return r 电阻
	 */
	public static double getRCR(double f, double c) {
		//r = 1 / f / PI2 / c;
		double pic = DoubleUtils.divide(1.0, f, 10);
		double fc = DoubleUtils.divide(pic, PI2, 10);
		return DoubleUtils.divide(fc, c);
	}
	/************RL滤波器相关计算*******************************
	 /******
	 * 计算RL滤波器频率方法
	 * @param r 电阻
	 * @param l 电感
	 * @return f 频率
	 */
	public static double getRLF(double r, double l) {
		//f = r/(PI2 * l);
		double pil = DoubleUtils.mul(PI2, l);
		return DoubleUtils.divide(r, pil);
	}
	/******
	 * 计算RL滤波器电感方法
	 * @param r 电阻
	 * @param f 频率
	 * @return l 电感
	 */
	public static double getRLL(double r, double f) {
		// l = r/f/PI2;
		double pil = DoubleUtils.divide(r, f, 10);
		return DoubleUtils.divide(pil, PI2);
	}
	/******
	 * 计算RL滤波器电阻方法
	 * @param l 电感
	 * @param f 频率
	 * @return r 电阻
	 */
	public static double getRLR(double l, double f) {
		// r = (PI2 * l) * f
		double pil = DoubleUtils.mul(PI2, l);
		return  DoubleUtils.mul(pil, f);
	}
	/************************RLC滤波器相关计算************
	 /*****
	 * 计算RLC频率方法
	 * @param l 电感
	 * @param c 电容
	 * @return f 频率
	 */
	public static double getRLCF(double l, double c) {
		//f = 1/(PI2 * Math.sqrt(l * c));
		double lc = Math.sqrt(l * c);
		double plc = DoubleUtils.mul(PI2, lc);
		return DoubleUtils.divide(1.0, plc);
	}

	/*****
	 * 计算RLC电感方法
	 * @param f 频率
	 * @param c 电容
	 * @return l 电感
	 */
	public static double getRLCL(double f, double c) {
//		var v = 1/f/PI2;
//		l = (v * v)/c
		double yf = DoubleUtils.divide(1.0, f, 10);
		double v = DoubleUtils.divide(yf, PI2, 10);
		double vh = DoubleUtils.mul(v, v);
		return DoubleUtils.divide(vh, c);
	}
	/*****
	 * 计算RLC电容方法
	 * @param l 电感
	 * @param f 频率
	 * @return c 电容
	 */
	public static double getRLCC(double l, double f) {
//		var v = 1/f/PI2;
//		c = (v * v)/l
		double yf = DoubleUtils.divide(1.0, f, 10);
		double v = DoubleUtils.divide(yf, PI2, 10);
		double vh = DoubleUtils.mul(v, v);
		return DoubleUtils.divide(vh, l);
	}

	public static void main(String[] args) {
		//其它方法已经在js里面验算，抽样测试
		double rc = getRCC(2,1);
		System.out.println( UnitConversion.format(new UnitConversion(rc).get_m(), 3));

		double rl = getRLR(2,1);
		System.out.println( UnitConversion.format(new UnitConversion(rl).get_m(), 3));

		//设置电感的值
		double l = 1.0;
		//设置电容的值，单位是PH， 换算成 H
		double c = new UnitConversion().set_p(1).getValue();
		//求 频率
		double f = getRLCF(l,c);
		System.out.println( UnitConversion.format(new UnitConversion(f).getK(), 3));

		double ec = getRLCC(1,1);
		System.out.println( UnitConversion.format(new UnitConversion(ec).get_m(), 3));

		double el = getRLCL(1,2);
		System.out.println( UnitConversion.format(new UnitConversion(el).get_m(), 3));

	}
}
