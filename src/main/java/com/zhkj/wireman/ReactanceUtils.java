package com.zhkj.wireman;

import com.zhkj.wireman.unit.UnitConversion;

/**
 * 电抗/谐振计算器
 */
public class ReactanceUtils {
    static Double PI = Math.PI;
    static Double PI2 = PI * 2;
    /*****************************容抗计算器
    /**
     *  获取容抗计算
     * @param c 电容
     * @param f 频率
     * @return  xc 容抗
     */
    public static double getCFXc(double c, double f){
        double pi2F = DoubleUtils.mul(PI2, f);
        double pi2Fc = DoubleUtils.mul(pi2F, c);
        return DoubleUtils.divide(1.0, pi2Fc);
    }
    /**
     * 获取频率计算
     * @param c 电容
     * @param xc 容抗
     * @return f 频率
     */
    public static double getCXcF(double c, double xc){
        double pic = DoubleUtils.divide(1.0, xc, 10);
        double fc = DoubleUtils.divide(pic, PI2, 10);
        return DoubleUtils.divide(fc, c);
    }
    /**
     *  获取电容计算
     * @param f 频率
     * @param xc 容抗
     * @return c 电容
     */
    public static double getFXcC(double f, double xc){
        double pic = DoubleUtils.divide(1.0, xc, 10);
        double fc = DoubleUtils.divide(pic, PI2, 10);
        return DoubleUtils.divide(fc, f);
    }

    /******************************感抗计算器
    /**
     *  获取感抗
     * @param l 电感
     * @param f 频率
     * @return  XL 感抗
     */
    public static double getLFXl(double l, double f){
        double pi2F = DoubleUtils.mul(PI2, f);
        return DoubleUtils.mul(pi2F, l);
    }
    /**
     * 获取频率
     * @param l 电感
     * @param xl    感抗
     * @return f 频率
     */
    public static double getLXlF(double l, double xl){
        double pi2F = DoubleUtils.divide(xl, l, 10);
        return DoubleUtils.divide(pi2F, PI2);
    }
    /**
     *  获取电感
     * @param f 频率
     * @param xl    感抗
     * @return  l 电感
     */
    public static double getFXlL(double f, double xl){
        double pi2L = DoubleUtils.divide(xl, f, 10);
        return DoubleUtils.divide(pi2L, PI2);
    }

    /************************谐振计算  or共振模式
     /*****
     * 获取共振频率算法
     * @param l 电感
     * @param c 电容
     * @return f0 频率
     */
    public static double getLCF0(double l, double c) {
        double lc = Math.sqrt(DoubleUtils.mul(l, c));
        double plc = DoubleUtils.mul(PI2, lc);
        return DoubleUtils.divide(1.0, plc);
    }

    /*****
     * 获取电感计算方法
     * @param f0 频率
     * @param c 电容
     * @return l 电感
     */
    public static double getF0CL(double f0, double c) {
        double yf = DoubleUtils.divide(1.0, f0, 10);
        double v = DoubleUtils.divide(yf, PI2, 10);
        double vh = DoubleUtils.mul(v, v);
        return DoubleUtils.divide(vh, c);
    }
    /*****
     * 获取电容的方法
     * @param l 电感
     * @param f0 频率
     * @return c 电容
     */
    public static double getF0LC(double l, double f0) {
        double yf = DoubleUtils.divide(1.0, f0, 10);
        double v = DoubleUtils.divide(yf, PI2, 10);
        double vh = DoubleUtils.mul(v, v);
        return DoubleUtils.divide(vh, l);
    }

    public static void main(String[] args) {
        System.out.println(new UnitConversion(getCFXc(1.0,1.0)).get_m());
        System.out.println(new UnitConversion(getCXcF(1.0,1.0)).get_m());
        System.out.println(new UnitConversion(getFXcC(2.0,1.0)).get_m());
        System.out.println("---------------------------------------");
        System.out.println(new UnitConversion(getLFXl(2.0,3.0)).getValue());
        System.out.println(new UnitConversion(getLXlF(2.0,3.0)).get_m());
        System.out.println(new UnitConversion(getFXlL(2.0,3.0)).get_m());
        System.out.println("---------------------------------------");
        System.out.println(new UnitConversion(getLCF0(2.0,3.0)).getValue());
        System.out.println(new UnitConversion(getF0CL(2.0,3.0)).get_m());
        System.out.println(new UnitConversion(getF0LC(2.0,3.0)).get_m());
    }
}
