package com.zhkj.wireman;

/**
 * Y 三角变换
 */
public class YTriangleUtils {
    /**
     *  △-Y转换公式  求r1
     * @param ra 电阻 a
     * @param rb 电阻 a
     * @param rc 电阻 a
     * @return r1 电阻
     */
    public static double getRaRbRcR1(double ra, double rb, double rc){
        double rbRc = DoubleUtils.mul(rb, rc);

        double raRb = DoubleUtils.add(ra, rb);
        double raRbRc = DoubleUtils.add(raRb, rc);
        return DoubleUtils.divide(rbRc, raRbRc);
    }
    /**
     *  △-Y转换公式  求r2
     * @param ra 电阻 a
     * @param rb 电阻 a
     * @param rc 电阻 a
     * @return r1 电阻
     */
    public static double getRaRbRcR2(double ra, double rb, double rc){
        double raRc = DoubleUtils.mul(ra, rc);

        double raRb = DoubleUtils.add(ra, rb);
        double raRbRc = DoubleUtils.add(raRb, rc);
        return DoubleUtils.divide(raRc, raRbRc);
    }
    /**
     *  △-Y转换公式  求r3
     * @param ra 电阻 a
     * @param rb 电阻 a
     * @param rc 电阻 a
     * @return r1 电阻
     */
    public static double getRaRbRcR3(double ra, double rb, double rc){
        double raRc = DoubleUtils.mul(ra, rb);

        double raRb = DoubleUtils.add(ra, rb);
        double raRbRc = DoubleUtils.add(raRb, rc);
        return DoubleUtils.divide(raRc, raRbRc);
    }

    /**
     *  Y-△转换公式： 求ra
     * @param r1 电阻 1
     * @param r2 电阻 2
     * @param r3 电阻 3
     * @return ra
     */
    public static double getR1R2R3Ra(double r1, double r2, double r3){
        double r1r2Ar2r3Ar3r1 = getR1r2Ar2r3Ar3r1(r1, r2, r3);

        return DoubleUtils.divide(r1r2Ar2r3Ar3r1, r1);
    }
    /**
     *  Y-△转换公式： 求rb
     * @param r1 电阻 1
     * @param r2 电阻 2
     * @param r3 电阻 3
     * @return ra
     */
    public static double getR1R2R3Rb(double r1, double r2, double r3){
        double r1r2Ar2r3Ar3r1 = getR1r2Ar2r3Ar3r1(r1, r2, r3);

        return DoubleUtils.divide(r1r2Ar2r3Ar3r1, r2);
    }
    /**
     *  Y-△转换公式： 求rc
     * @param r1 电阻 1
     * @param r2 电阻 2
     * @param r3 电阻 3
     * @return ra
     */
    public static double getR1R2R3Rc(double r1, double r2, double r3){
        double r1r2Ar2r3Ar3r1 = getR1r2Ar2r3Ar3r1(r1, r2, r3);

        return DoubleUtils.divide(r1r2Ar2r3Ar3r1, r3);
    }

    public static double getR1r2Ar2r3Ar3r1(double r1, double r2, double r3) {
        double r1r2 = DoubleUtils.mul(r1, r2);
        double r2r3 = DoubleUtils.mul(r2, r3);
        double r3r1 = DoubleUtils.mul(r3, r1);

        double r1r2Ar2r3 = DoubleUtils.add(r1r2, r2r3);
        return DoubleUtils.add(r1r2Ar2r3, r3r1);
    }


    public static void main(String[] args) {
        System.out.println(getRaRbRcR1(44, 44, 12));
        System.out.println(getR1R2R3Ra(44, 44, 4));
    }
}
